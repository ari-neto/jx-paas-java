# paas-java

## show dependencies
~~~sh
mvn dependency:tree
~~~

## install dependencies
~~~sh
mvn install
~~~

## run tests
~~~sh
mvn test
~~~

## Build and Deploy

### Create jar file and Run

~~~sh
mvn package
java -jar cf push paas-java -p target/paas-java-0.0.1-SNAPSHOT.jar
~~~


### Run from Spring-Boot

~~~sh
 mvn spring-boot:run
 ~~~

### Deploy on Cloudfoundry

~~~sh
mvn package
cf push paas-java -p target/paas-java-0.0.1-SNAPSHOT.jar
~~~

### Build and Run using Docker

~~~sh
docker build -t paas-java .
docker run --rm --name paas-java -p 8080:8080 -d paas-java
~~~

### Build and Run using docker-compose

~~~sh
docker build -t paas-java .
docker-compose up
~~~

### Build and Run using Kubernetes

~~~sh
sh build.sh
kubectl create -f k8s-deploy.yml
~~~

### SonarQube
~~~sh
sonar   -Dsonar.host.url=http://<server>:9000   -Dsonar.login=<token>
~~~