FROM maven:alpine
COPY . /usr/
WORKDIR /usr/
RUN mvn package
RUN ls
CMD ["java", "-jar", "target/paas-java-0.0.1-SNAPSHOT.jar"]
EXPOSE 8080